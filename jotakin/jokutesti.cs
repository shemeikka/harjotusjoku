﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jotakin
{
    [TestFixture]
    class jokutesti
    {
        [TestCase]

        public void Plussa()
        {
            jokuclass math = new jokuclass();
            Assert.AreEqual(31, math.Plussa(20, 11));
        }
        [TestCase]

        public void  Miinus()
        {
            jokuclass math = new jokuclass();
            Assert.AreEqual(10, math.Miinus(20, 10));
        }
        [TestCase]
        public void Kerto()
        {
            jokuclass math = new jokuclass();
            Assert.AreEqual(36, math.Kerto(6, 6));
        }
        [TestCase]
        public void Jako()
        {
            jokuclass math = new jokuclass();
            Assert.AreEqual(5, math.Jako(30, 6));
        }
        [TestCase]
        public void Jakojäännös()
        {
            jokuclass math = new jokuclass();
            Assert.AreEqual(2, math.Jako(12, 5));
        }
    }
}
